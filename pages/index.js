import React from 'react'
import Sidebar from '../components/Sidebar'
import Player from '../components/Player'
import Center from '../components/Center'
import { getSession } from 'next-auth/react'
import Head from 'next/head'

export default function index() {
  return (
    <>
      <Head>
        <title>Tribikram's Spotify</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <meta property="og:description" content="This is the clone project of spotify that i made for my portfolio project."/>
        <meta property="og:image" content="https://i.scdn.co/image/ab6775700000ee85f9398258712a4f7f91906abc"/>
      </Head>
      <div className='bg-black h-screen overflow-hidden'>
        <main className='flex'>
          <Sidebar />
          <Center />
        </main>
        <div className='sticky bottom-0'>
          <Player />
        </div>
      </div>
    </>
  )
}

export async function getServerSideProps(context){
  const session = await getSession(context);

  return{
    props:{
      session,
    }
  }
}